FROM python:3.10.1-alpine3.15 as build

RUN apk add \ 
    gcc \
    python3-dev \
    musl-dev \
    postgresql-dev \
    build-base

WORKDIR /python-deps

COPY requirements.txt /

RUN pip install --prefix=/python-deps -r /requirements.txt


FROM python:3.10.1-alpine3.15

ENV GUNICORN_WORKERS=1
ENV GUNICORN_THREADS=4

COPY --from=build /python-deps /usr/local                                                                                       

RUN apk --no-cache add libpq

WORKDIR /app

COPY app.py run.sh ./
COPY routines /app/routines



CMD /app/run.sh
